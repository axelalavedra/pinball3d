﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour {

    GameObject cam;
    Rigidbody rbd;
    bool spawning;
    float counter;
    AudioSource[] audios;

	// Use this for initialization
	void Start () {
        cam = GameObject.Find("Main Camera");
        rbd = GetComponent<Rigidbody>();
        counter = 0;
        audios = GetComponents<AudioSource>();
	}
    void Update()
    {
        if(rbd.useGravity && counter==0)
        {
            if (Input.GetKeyDown(KeyCode.Q)) {
                rbd.AddForce(-60, 0, 125);
                counter = 0.5f;
                StartCoroutine(Shake("right"));
                audios[0].Play();
                return;
            }
            if (Input.GetKeyDown(KeyCode.E)) {
                rbd.AddForce(60, 0, 125);
                counter = 0.5f;
                StartCoroutine(Shake("left"));
                audios[0].Play();
            }
        }
        if (counter > 0)
        {
            counter -= 1 * Time.deltaTime;
            if (counter < 0) counter = 0;
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.name == "DeathZone")
        {
            audios[1].Play();
            rbd.useGravity = false;
            rbd.velocity = Vector3.zero;
            GameManager.deadBall();
        }     
    }
    IEnumerator Shake(string dir)
    {
        if(dir == "right")
        {
            cam.transform.localPosition = new Vector3(0.05f, 1, -3.1f);
            yield return new WaitForSeconds(0.07f);
            cam.transform.localPosition = new Vector3(0, 1, -3.1f);
        }
        else
        {
            cam.transform.localPosition = new Vector3(-0.05f, 1, -3.1f);
            yield return new WaitForSeconds(0.07f);
            cam.transform.localPosition = new Vector3(0, 1, -3.1f);
        }
    }
}
