﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BumperController : MonoBehaviour {

    private Rigidbody Ballrbd;
    private AudioSource sound;
    private Light my_light;

    void Start()
    {
        Ballrbd = GameObject.Find("Ball").GetComponent<Rigidbody>();
        sound = GetComponent<AudioSource>();
        my_light = transform.GetComponentInChildren<Light>();
        my_light.enabled = false;
    }

    void OnCollisionEnter(Collision col)
    {
        my_light.enabled = true;
        sound.Play();
        Vector3 force = Ballrbd.velocity.normalized * 3.5f;
        //Ballrbd.AddForce(force, ForceMode.Impulse);
        Ballrbd.AddExplosionForce(force.magnitude, transform.position, 5f, 0f, ForceMode.Impulse);
        GameManager.increaseScore(100);
        transform.DOShakeScale(10f * Time.deltaTime, 0.5f, 5, 0, true);        
        Invoke("stopLight", 0.1f);
    }
    void stopLight()
    {
        my_light.enabled = false;
    }
}
