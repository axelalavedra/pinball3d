﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Buttons : MonoBehaviour {

    private GameObject ovniSpawner, button, HUD;
    private Text mainText;
    private AudioSource audio;

    // Use this for initialization
    void Start () {
        ovniSpawner = GameObject.Find("SpawnerOvni");
        HUD = GameObject.Find("Numbers");
        mainText = GameObject.Find("MainText").GetComponent<Text>();
        button = GameObject.Find("Button");
        audio = GetComponent<AudioSource>();
    }

    public void initializeGame()
    {
        audio.Play();
        mainText.text = "";
        mainText.DOText("Initializing...", 50f * Time.deltaTime);
        button.transform.DORotate(new Vector3(0, 0, -1800), 150f * Time.deltaTime);
        button.GetComponent<Button>().enabled = false;

        StartCoroutine(createHUD());

    }

    IEnumerator createHUD()
    {
        foreach (Transform child in HUD.transform)
        {
            Destroy(child.gameObject);
        }
        GameManager.initialize();

        Color32 color = new Color32(9,172,255,255);

        GameObject Score = new GameObject("Score");
        Score.AddComponent<Text>();
        Score.GetComponent<Text>().font = (Font)Resources.Load("Trench");
        Score.GetComponent<Text>().fontSize = 45;
        Score.GetComponent<Text>().color = color;
        Score.transform.SetParent(HUD.transform, false);

        GameObject ScoreNum = new GameObject("ScoreNum");  
        ScoreNum.AddComponent<Text>();
        ScoreNum.GetComponent<Text>().font = (Font)Resources.Load("Trench");
        ScoreNum.GetComponent<Text>().fontSize = 45;
        ScoreNum.GetComponent<Text>().color = color;
        GameManager.setScoreText(ScoreNum);
        ScoreNum.transform.SetParent(HUD.transform, false);

        GameObject Lives = new GameObject("Lives");
        Lives.AddComponent<Text>();
        Lives.GetComponent<Text>().font = (Font)Resources.Load("Trench");
        Lives.GetComponent<Text>().fontSize = 45;
        Lives.GetComponent<Text>().color = color;
        Lives.transform.SetParent(HUD.transform, false);

        GameObject LivesNum = new GameObject("LivesNum");
        LivesNum.AddComponent<Text>();
        LivesNum.GetComponent<Text>().font = (Font)Resources.Load("Trench");
        LivesNum.GetComponent<Text>().fontSize = 45;
        LivesNum.GetComponent<Text>().color = color;
        GameManager.setLivesText(LivesNum);
        LivesNum.transform.SetParent(HUD.transform, false);


        Score.GetComponent<Text>().DOText("Score: ", 50f * Time.deltaTime);
        ScoreNum.GetComponent<Text>().DOText("0", 50f * Time.deltaTime);
        yield return new WaitForSeconds(1f);
        Lives.GetComponent<Text>().DOText("Lives: ", 50f * Time.deltaTime);
        LivesNum.GetComponent<Text>().DOText(GameManager.getLives().ToString(), 50f * Time.deltaTime);

        mainText.DOKill();
        mainText.text = "";
        button.transform.DOScale(new Vector3(0, 0, 0), 50f * Time.deltaTime);
        yield return new WaitForSeconds(1f);

        ovniSpawner.GetComponent<OvniSpawner>().setMoving(true);
        button.transform.DOKill();
        audio.Stop();
        button.SetActive(false);    
    }
}
