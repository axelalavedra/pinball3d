﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipperControl : MonoBehaviour {

    public string flipperButton;
    private Rigidbody rbd;
    private AudioSource sound;

    // Use this for initialization
    void Start () {
        rbd = GetComponent<Rigidbody>();
        sound = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (Input.GetButton(flipperButton)) rbd.AddForce(new Vector3(0, 0, 40), ForceMode.Impulse);
        else rbd.AddForce(new Vector3(0, 0, -40), ForceMode.Impulse);
    }
    void Update()
    {
        if (Input.GetButtonDown(flipperButton)) sound.Play();
    }

}
