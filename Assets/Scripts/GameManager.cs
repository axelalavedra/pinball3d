﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GameManager : MonoBehaviour {

    private static int score, lives, targets_down;
    private static GameObject spawnerOvni, button;
    public static TargetController[] targets;
    private static Text scoreText, livesText, mainText;
    private static AudioSource[] audios;
    static public GameManager instance;

    // Use this for initialization
    void Start () {
        spawnerOvni = GameObject.Find("SpawnerOvni");
        mainText = GameObject.Find("MainText").GetComponent<Text>();
        button = GameObject.Find("Button");
        audios = GetComponents<AudioSource>();
        lives = 3;
        score = 0;
        instance = this;

        Transform targs = GameObject.Find("Targets").transform;
        targets = new TargetController[targs.childCount];
        for(int i = 0; i< targs.childCount; ++i)
        {
            targets[i] = targs.GetChild(i).GetComponent<TargetController>();
        }
	}

    public static void increaseScore(int points)
    {
        score += points;
        scoreText.text = score.ToString();
    }
    public static void deadBall()
    {
        lives--;
        livesText.text = lives.ToString();
        if (lives == 0)
        {
            audios[0].Play();
            mainText.DOText("Game Over\r\nPress button to Restart", 50f * Time.deltaTime);
            button.SetActive(true);
            button.GetComponent<Button>().enabled = true;
            button.transform.DOScale(new Vector3(1, 1, 1), 50f * Time.deltaTime);         
        }
        else spawnerOvni.GetComponent<OvniSpawner>().setMoving(true);
    }
    public static int getLives()
    {
        return lives;
    }

    public static void setScoreText(GameObject st)
    {
        scoreText = st.GetComponent<Text>();
    }
    public static void setLivesText(GameObject lt)
    {
        livesText = lt.GetComponent<Text>();
    }

    public static void targetDown()
    {
        targets_down++;
        if(targets_down==3)
        {
            audios[1].Play();
            resetTargets();
            instance.StartCoroutine("lightShow");
            score = score * 3;
            scoreText.text = score.ToString();
            targets_down = 0;
        }
    }

    private static void resetTargets()
    {
        foreach(TargetController t in targets)
        {
            t.setReturning(true);
            t.setActiveLight(true);
        }
    }
    public static void initialize()
    {
        lives = 3;
        score = 0;
        targets_down = 0;
        resetTargets();
    }
    IEnumerator lightShow()
    {
        GameObject tableLights = GameObject.Find("TableLights");
        Light[] lights = tableLights.GetComponentsInChildren<Light>();
        for(int i = 0; i<5;++i)
        {
            foreach (Light l in lights)
            {
                l.enabled = false;
            }
            yield return new WaitForSeconds(0.2f);
            foreach (Light l in lights)
            {
                l.enabled = true;
            }
            yield return new WaitForSeconds(0.2f);
        }    
    }
}
