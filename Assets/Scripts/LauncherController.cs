﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LauncherController : MonoBehaviour {

    Vector3 maxPos, startPos;
    bool ballReady, loaded;
    private Rigidbody ballRbd;
    private float counter;
    private AudioSource sound;

	// Use this for initialization
	void Start () {
        ballRbd = GameObject.Find("Ball").GetComponent<Rigidbody>();
        ballReady = false;
        loaded = false;
        counter = 0;
        startPos = transform.localPosition;
        maxPos = new Vector3(startPos.x, startPos.y, startPos.z + 0.5f);
        sound = GetComponent<AudioSource>();
    }
	
    void FixedUpdate()
    {
        if (Input.GetButton("LauncherButton"))
        {
            if (transform.localPosition != maxPos)
            {
                transform.localPosition = Vector3.MoveTowards(transform.localPosition, maxPos, 0.5f * Time.deltaTime);
                counter += 5 * Time.deltaTime;
                loaded = true;
            }
        }
        else if (counter > 0)
        {
            if (ballReady && loaded)
            {
                ballRbd.AddForce(0, 0, counter * 150);
                ballReady = false;
                loaded = false;
            }
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, startPos, 5f * Time.deltaTime);
            counter -= 5 * Time.deltaTime;
        }
    }
	// Update is called once per frame
	void Update () {

       
        if (Input.GetButtonUp("LauncherButton")) sound.Play();
        if (counter < 0)
        {
            counter = 0;
            loaded = false;
        }

	}
    void OnCollisionEnter(Collision col)
    {
        ballReady = true;
    }
    void OnCollisionExit(Collision col)
    {
        ballReady = false;
    }
}
