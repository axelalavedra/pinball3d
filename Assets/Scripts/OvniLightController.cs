﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OvniLightController : MonoBehaviour {

    private bool scoring;
    private int actual_light;
    public Light[] lights;
    private AudioSource sound;
	// Use this for initialization
	void Start () {
        actual_light = 0;
        sound = GetComponent<AudioSource>();
	}

    void OnTriggerEnter(Collider col)
    {
        if (!sound.isPlaying) sound.Play();
        StartCoroutine(lightShow());
    }

    IEnumerator lightShow()
    {
        int total_lights = lights.Length;
        lights[actual_light].enabled = true;
        yield return new WaitForSeconds(0.07f);

        for (int i = 0; i<total_lights; ++i)
        {
            lights[actual_light].enabled = false;
            actual_light++;
            if(actual_light < total_lights) lights[actual_light].enabled = true;
            yield return new WaitForSeconds(0.07f);
        }
        actual_light = 0;
    }
}
