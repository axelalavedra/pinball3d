﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OvniRide : MonoBehaviour {

    private List<GameObject> points;
    private int point, total_points;
    private bool moving;
    private GameObject ball;
    private Rigidbody rbdBall;
    private GameObject trajectory;

	// Use this for initialization
	void Start () {
        moving = false;
        point = 0;
        ball = GameObject.Find("Ball");
        rbdBall = ball.GetComponent<Rigidbody>();
        trajectory = transform.GetChild(0).gameObject;
        total_points = trajectory.transform.childCount;
        points = new List<GameObject>();

        for(int i = 0; i<total_points; ++i)
        {
            points.Insert(i, trajectory.transform.GetChild(i).gameObject);
        }       
    }

    void FixedUpdate()
    {
        if (moving)
        {
            ball.transform.position = Vector3.MoveTowards(ball.transform.position, points[point].transform.position, 3f * Time.deltaTime);
            if (ball.transform.position == points[point].transform.position) point++;
            if (point == total_points)
            {
                moving = false;
                rbdBall.useGravity = true;
                rbdBall.AddForce(-250f, 0, 90f);
            }
        }
    }
	
    void OnTriggerEnter(Collider col)
    {
        moving = true;
        point = 0;
        rbdBall.useGravity = false;
        rbdBall.velocity = Vector3.zero;
        rbdBall.angularVelocity = Vector3.zero;
        if(name =="PinballEntrance") GameManager.increaseScore(150);
    }
}
