﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OvniSpawner : MonoBehaviour {

    private bool moving;
    private int phase;
    Vector3 stopPosition, startPosition;
    GameObject body, ball, spawnPosition, finishPosition;
    Light myLight;
    public AudioSource[] audios;

	// Use this for initialization
	void Start () {
        phase = 0;
        startPosition = transform.localPosition;
        stopPosition = new Vector3(startPosition.x + 3.465f, startPosition.y, startPosition.z);
        spawnPosition = transform.GetChild(1).gameObject;
        finishPosition = transform.GetChild(2).gameObject;
        body = transform.GetChild(0).gameObject;
        ball = GameObject.Find("Ball");
        myLight = transform.GetChild(3).GetComponent<Light>();

        audios = GetComponents<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
        if(moving)
        {
            body.transform.Rotate(new Vector3(0, 0, 180) * Time.deltaTime, Space.Self);
            switch (phase)
            {
                case 0:
                    {
                        if(!audios[0].isPlaying) audios[0].Play();
                        transform.localPosition = Vector3.MoveTowards(transform.localPosition, stopPosition, 2f * Time.deltaTime);
                        if (transform.localPosition == stopPosition)
                        {
                            phase = 1;
                            myLight.enabled = true;
                            ball.transform.position = spawnPosition.transform.position;
                            audios[0].Stop();
                        }
                    }
                    break;
                case 1:
                    {
                        if (!audios[1].isPlaying) audios[1].Play();
                        ball.transform.position = Vector3.MoveTowards(ball.transform.position, finishPosition.transform.position, 0.5f * Time.deltaTime);
                        if (ball.transform.position == finishPosition.transform.position)
                        {
                            ball.GetComponent<Rigidbody>().useGravity = true;
                            phase = 2;
                            myLight.enabled = false;
                            audios[1].Stop();
                        }
                    }
                    break;
                case 2:
                    {
                        if (!audios[2].isPlaying) audios[2].Play();
                        transform.localPosition = Vector3.MoveTowards(transform.localPosition, startPosition, 2f * Time.deltaTime);
                        if (transform.localPosition == startPosition)
                        {
                            phase = 0;
                            moving = false;
                            audios[2].Stop();
                        }
                    }
                    break;
            }
        }            
    }

    public void setMoving(bool m)
    {
        moving = m;
    }
}
