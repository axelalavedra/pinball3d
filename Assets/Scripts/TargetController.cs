﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetController : MonoBehaviour {

    private Vector3 startPos, maxPos;
    private bool hitted, returning;
    private Light mylight;
    private AudioSource sound;

    // Use this for initialization
    void Start () {
        startPos = transform.localPosition;
        maxPos = new Vector3(startPos.x, startPos.y - 0.4f, startPos.z);
        hitted = false;
        mylight = transform.GetChild(0).GetComponent<Light>();
        sound = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		if(hitted)
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, maxPos, 0.2f * Time.deltaTime);
            if (transform.localPosition == maxPos)
            {
                hitted = false;
                GameManager.targetDown();
            }
        }
        else if(returning)
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, startPos, 0.15f * Time.deltaTime);
            if (transform.localPosition == startPos) returning = false;
        }
	}

    void OnCollisionEnter(Collision col)
    {
        if(!hitted)
        {
            if (!sound.isPlaying) sound.Play();
            GameManager.increaseScore(150);
            hitted = true;
            mylight.enabled = false;
        }   
    }
    public void setReturning(bool r)
    {
        returning = r;
    }
    public void setActiveLight(bool a)
    {
        mylight.enabled = a;
    }
}
